# texlive.profile written on Fri May 19 12:30:28 2017 UTC
# It will NOT be updated and reflects only the
# installation profile at installation time.
selected_scheme scheme-custom
TEXDIR $TX_INSTALLPATH/$TX_VERSION
TEXMFCONFIG ~/.texlive$TX_VERSION/texmf-config
TEXMFHOME $TX_MFHOME
TEXMFLOCAL $TX_INSTALLPATH/texmf-local
TEXMFSYSCONFIG $TX_INSTALLPATH/$TX_VERSION/texmf-config
TEXMFSYSVAR $TX_INSTALLPATH/$TX_VERSION/texmf-var
TEXMFVAR ~/.texlive$TX_VERSION/texmf-var
binary_$TX_BINFLAVOUR 1
collection-basic 1
collection-bibtexextra 1
collection-fontsrecommended 1
collection-langenglish 1
collection-langgerman 1
collection-latex 1
collection-latexextra 1
collection-latexrecommended 1
collection-mathscience 1
collection-metapost 1
collection-pictures 1
collection-xetex 1
instopt_adjustpath 1
instopt_adjustrepo 1
instopt_letter 0
instopt_portable 0
instopt_write18_restricted 1
tlpdbopt_autobackup 1
tlpdbopt_backupdir tlpkg/backups
tlpdbopt_create_formats 1
tlpdbopt_desktop_integration 1
tlpdbopt_file_assocs 1
tlpdbopt_generate_updmap 0
tlpdbopt_install_docfiles 0
tlpdbopt_install_srcfiles 0
tlpdbopt_post_code 1
tlpdbopt_sys_bin $LOCAL_BINPATH
tlpdbopt_sys_info /usr/local/share/info
tlpdbopt_sys_man /usr/local/share/man
tlpdbopt_w32_multi_user 1


